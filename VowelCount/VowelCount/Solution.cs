﻿using System.Linq;

namespace VowelCount
{
    public class Solution
    {
        public int GetVowelCount(string str)
        {
            int vowelCount = 0;
            int[] vowel = {'a', 'e', 'i', 'o', 'u'};

            if (str.Replace(" ", "").Length == 0)
            {
                return 0;
            }

            foreach (var ch in str)
            {
                if (vowel.Contains(ch))
                    vowelCount++;
            }

            return vowelCount;
        }
    }
}
