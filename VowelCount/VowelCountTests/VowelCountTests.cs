﻿using NUnit.Framework;
using VowelCount;

namespace VowelCountTests
{
    public class VowelCountTests
    {

        [TestCase("", Description = "Null string")]
        [TestCase(" ", Description = "Space string")]
        [TestCase("sklpr382740vnnbc", Description = "No vowel")]
        [TestCase("jdkfpEfjgUApllIldfO", Description = "No lowercase vowel")]
        public void TestResultIsZero(string str)
        {
            Solution solution = new Solution();
            Assert.AreEqual(0, solution.GetVowelCount(str));
        }

        [TestCase("abracadabra", Description = "There_are_five_a")]
        [TestCase("sdfjhkeskdhfesldkjesldkhfelskdjfe", Description = "There_are_five_e")]
        [TestCase("igfghitfghi345icbvcbi ", Description = "There_are_five_i")]
        [TestCase("vbnxozxcozzzo234obcco", Description = "There_are_five_o")]
        [TestCase("sdggudfjjurtyuvbnuxxxu", Description = "There_are_five_u")]
        public void TestContainsOneTypeVowel(string str)
        {
            Solution solution = new Solution();
            Assert.AreEqual(5, solution.GetVowelCount(str));
        }

        [TestCase("aEIOU", Description = "Contains uppercase EIOU")]
        [TestCase("AeIOU", Description = "Contains uppercase AIOU")]
        [TestCase("AEiOU", Description = "Contains uppercase AEOU")]
        [TestCase("AEIoU", Description = "Contains uppercase AEIU")]
        [TestCase("AEIOu", Description = "Contains uppercase AEIO")]
        public void TestContainsUppercaseVowel(string str)
        {
            Solution solution = new Solution();
            Assert.AreEqual(1, solution.GetVowelCount(str));
        }
    }
}
