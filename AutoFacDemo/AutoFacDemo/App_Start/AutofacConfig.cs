﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using AutoFacDemo.Models;
using WebGrease.Configuration;

namespace AutoFacDemo.App_Start
{
    public class AutofacConfig
    {
        public static void BootStrapper()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<GameValidator>().As<IValidator>().PropertiesAutowired();
            //builder.RegisterType<Account>().As<IAccount>().InstancePerLifetimeScope();
            //builder.RegisterType<Hash>().As<IHash>().InstancePerLifetimeScope();


            var container = builder.Build();
            
            var config = GlobalConfiguration.Configuration;
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}