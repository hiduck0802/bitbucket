﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoFacDemo.Models
{
    public interface IAccount
    {
        string getPassword(string id);
    }
    public class Account : IAccount
    {
        public string getPassword(string id)
        {
            throw new NotImplementedException();
        }
    }

    public class StubAccount : IAccount
    {
        public string getPassword(string id)
        {
            return "123";
        }
    }
}