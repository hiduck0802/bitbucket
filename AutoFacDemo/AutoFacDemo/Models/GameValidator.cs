﻿using System.Collections.Generic;
using AutoFacDemo.Controllers;
using NUnit.Framework;

namespace AutoFacDemo.Models
{
    public interface IValidator
    {
        bool checkAuthentication(string id, string password);
    }

    public class GameValidator : IValidator
    {
        protected List<int> AcceptId;
        private IAccount _Account;
        private IHash _Hash;

        public GameValidator(IAccount account, IHash hash)
        {
            this._Account = account;
            this._Hash = hash;
        }
        public bool checkAuthentication(string id, string password)
        {
            if (!AcceptId.Contains(int.Parse(id)))
            {
                return false;
            }

            /* get password by id from DB */
            var passwordFromDB = this._Account.getPassword(id);

            /* do hash calculate */
            var hashResult = this._Hash.GetHashResult(passwordFromDB);

            /* check if consistent */
            return hashResult == password;
        }
    }

    public class FSValidator : GameValidator
    {
        public FSValidator(IAccount account, IHash hash) : base(account, hash)
        {
            AcceptId = new List<int>() { 111, 222 };
        }
    }

    [TestFixture]
    public class GameValidatorTest
    {
        [Test]
        public void checkAuthenticationTest()
        {
            IAccount account = new StubAccount();
            IHash hash = new StubHash();

            GameValidator liveGameValidator = new FSValidator(account, hash);
            string id = "111";
            string pwd = "222";
            Assert.AreEqual(liveGameValidator.checkAuthentication(id, pwd), true);
        }

        [Test]
        public void checkIdTest()
        {
            IAccount account = new StubAccount();
            IHash hash = new StubHash();

            GameValidator liveGameValidator = new FSValidator(account, hash);
            string id = "333";
            string pwd = "222";
            Assert.AreEqual(liveGameValidator.checkAuthentication(id, pwd), false);
        }

        [Test]
        public void test()
        {
            IAccount account = new StubAccount();
            IHash hash = new StubHash();

            var fsController = new FSController();
            string id = "333";
            string pwd = "222";
            Assert.AreEqual(fsController.AccountAuthentication(id, pwd), false);
        }
    }
}