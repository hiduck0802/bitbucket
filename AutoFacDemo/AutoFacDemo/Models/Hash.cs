﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoFacDemo.Models
{
    public interface IHash
    {
        string GetHashResult(string passwordFromDB);
    }

    public class Hash : IHash
    {
        public string GetHashResult(string password)
        {
            throw new NotImplementedException();
        }
    }

    public class StubHash : IHash
    {
        public string GetHashResult(string password)
        {
            return "222";
        }
    }
}