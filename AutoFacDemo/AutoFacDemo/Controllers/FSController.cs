﻿using AutoFacDemo.Models;

namespace AutoFacDemo.Controllers
{
    public class FSController
    {
        private IValidator _Validator;
        public bool AccountAuthentication(string id, string pwd)
        {
            var account = new Account();
            var hash = new Hash();
            _Validator = new FSValidator(account, hash);
            return _Validator.checkAuthentication(id, pwd);
        }
    }
}