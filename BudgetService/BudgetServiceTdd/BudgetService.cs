﻿using System;
using System.Collections.Generic;

namespace BudgetServiceTdd
{
    public class BudgetService
    {
        private readonly List<Budget> _AllBudgeByYearMonth;
        private readonly Dictionary<int, int> _DaysOfMonth = new Dictionary<int, int>()
        {
            {1, 31 },
            {2, 28 },
            {3, 31 },
            {4, 30 },
            {5, 31 },
            {6, 30 },
            {7, 31 },
            {8, 31 },
            {9, 30 },
            {10, 31 },
            {11, 30 },
            {12, 31 },
        };

        public BudgetService(IBudgetRepository budgetRepository)
        {
            _AllBudgeByYearMonth = budgetRepository.GetAll();
        }

        public int GetMonth(string yearMonth)
        {
            return int.Parse(yearMonth.Substring(4, 2));
        }

        public double TotalAmount(DateTime start, DateTime end)
        {
            if (start > end) return 0;

            var startAdd = false;
            double totalBudge = 0;
            foreach (var budge in _AllBudgeByYearMonth)
            {
                var month = GetMonth(budge.YearMonth);
                if (month == start.Month)
                {
                    startAdd = true;
                }

                if (!startAdd) continue;
                
                var dayBudge = (double)budge.Amount / _DaysOfMonth[month];
                if (month == start.Month)
                {
                    var effectiveDays = _DaysOfMonth[start.Month] - start.Day;
                    totalBudge += dayBudge * effectiveDays;
                    if (start.Month == end.Month)
                    {
                        totalBudge += dayBudge;
                        break;
                    }
                }
                else if (month == end.Month)
                {
                    totalBudge += dayBudge * end.Day;
                    break;
                }
                else
                {
                    totalBudge += budge.Amount;
                }
            }

            return Math.Round(totalBudge, 2);
        }
    }
}