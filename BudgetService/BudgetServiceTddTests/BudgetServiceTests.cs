﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BudgetServiceTdd.Tests
{
    [TestClass]
    public class BudgetServiceTests
    {
        [TestMethod()]
        public void TotalAmount_Test30DayMonth_3000()
        {
            var budgeService = new BudgetService(new FakeBudgeRespository());
            var totalAmount = budgeService.TotalAmount(new DateTime(2018, 11, 1), new DateTime(2018, 11, 30));
            var expect = 3000;
            Assert.AreEqual(expect, totalAmount);
        }

        [TestMethod()]
        public void TotalAmount_Test31DayMonth_300()
        {
            var budgeService = new BudgetService(new FakeBudgeRespository());
            var totalAmount = budgeService.TotalAmount(new DateTime(2018, 10, 1), new DateTime(2018, 10, 31));
            var expect = 300;
            Assert.AreEqual(expect, totalAmount);
        }

        [TestMethod()]
        public void TotalAmount_TestDay_2900()
        {
            var budgeService = new BudgetService(new FakeBudgeRespository());
            var totalAmount = budgeService.TotalAmount(new DateTime(2018, 11, 2), new DateTime(2018, 11, 30));
            var expect = 2900;
            Assert.AreEqual(expect, totalAmount);
        }

        [TestMethod()]
        public void TotalAmount_TestStartOverEnd_0()
        {
            var budgeService = new BudgetService(new FakeBudgeRespository());
            var totalAmount = budgeService.TotalAmount(new DateTime(2018, 11, 28), new DateTime(2018, 11, 1));
            var expect = 0;
            Assert.AreEqual(expect, totalAmount);
        }

        [TestMethod()]
        public void TotalAmount_TestStartCrossOneMonth_3080()
        {
            var budgeService = new BudgetService(new FakeBudgeRespository());
            var totalAmount = budgeService.TotalAmount(new DateTime(2018, 11, 5), new DateTime(2018, 12, 10));
            double expect = 2500 + (double)1800 / 31 * 10;
            Assert.AreEqual(Math.Round(expect, 2), totalAmount);
        }

        [TestMethod()]
        public void TotalAmount_TestStartCrossTwoMonth_3832()
        {
            var budgeService = new BudgetService(new FakeBudgeRespository());
            var totalAmount = budgeService.TotalAmount(new DateTime(2018, 10, 5), new DateTime(2018, 12, 10));
            double expect = 3000 + (double)1800 / 31 * 10 + (double)300 / 31 * 26;
            Assert.AreEqual(Math.Round(expect, 2), totalAmount);
        }
    }

    public class FakeBudgeRespository : IBudgetRepository
    {
        public List<Budget> GetAll()
        {
            return new List<Budget>()
            {
                new Budget(){ Amount = 300, YearMonth = "201801" },
                new Budget(){ Amount = 600, YearMonth = "201802" },
                new Budget(){ Amount = 900, YearMonth = "201803" },
                new Budget(){ Amount = 1200, YearMonth = "201804" },
                new Budget(){ Amount = 1500, YearMonth = "201805" },
                new Budget(){ Amount = 300, YearMonth = "201806" },
                new Budget(){ Amount = 600, YearMonth = "201807" },
                new Budget(){ Amount = 900, YearMonth = "201808" },
                new Budget(){ Amount = 600, YearMonth = "201809" },
                new Budget(){ Amount = 300, YearMonth = "201810" },
                new Budget(){ Amount = 3000, YearMonth = "201811" },
                new Budget(){ Amount = 1800, YearMonth = "201812" },
            };
        }
    }
}