﻿namespace OnlyOne
{
    public class Solution
    {
        public bool OnlyOne(params bool[] flags)
        {
            int true_count = 0;
            foreach (var flag in flags)
            {
                if (flag == true)
                {
                    true_count += 1;
                }

                if (true_count == 2)
                    return false;
            }

            return true;
        }
    }
}
