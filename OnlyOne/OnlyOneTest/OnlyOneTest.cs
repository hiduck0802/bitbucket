﻿using NUnit.Framework;
using OnlyOne;

namespace OnlyOneTest
{
    
    public class OnlyOneTest
    {
        [TestCase(true, false)]
        [TestCase(false, true, false, false)]
        [TestCase(false, false, false, true)]
        public void ResultTrueTest(params bool[] flags)
        {
            Solution solution = new Solution();
            Assert.AreEqual(true, solution.OnlyOne(flags));
        }

        [TestCase(true, true)]
        [TestCase(true, false, true, false)]
        [TestCase(false, false, false, false)]
        public void ResultFalseTest(params bool[] flags)
        {
            Solution solution = new Solution();
            Assert.AreEqual(true, solution.OnlyOne(flags));
        }

        [Test]
        public void NullInputTest()
        {
            Solution solution = new Solution();
            Assert.AreEqual(true, solution.OnlyOne());
        }
    }
}
