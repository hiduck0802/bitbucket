﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace IOC_DI
{
    public class FootballStrikeController
    {
        public void AccountAuthentication(string id, string pwd)
        {
            var account = new Account();
            var hash = new Hash();
            var validator = new FootballStrikeValidator(account, hash);
            validator.checkAuthentication(id, pwd);
        }
    }
}
