﻿using System.Collections.Generic;
using NUnit.Framework;

namespace IOC_DI
{
    public interface IValidator
    {
        bool checkAuthentication(string id, string password);
    }

    public class LiveGameValidator : IValidator
    {
        protected List<int> AcceptId;
        private IAccount _Account;
        private IHash _Hash;

        public LiveGameValidator(IAccount account, IHash hash)
        {
            this._Account = account;
            this._Hash = hash;
        }
        public bool checkAuthentication(string id, string password)
        {
            if (!AcceptId.Contains(int.Parse(id)))
            {
                return false;
            }

            /* get password by id from DB */
            var passwordFromDB = this._Account.getPassword(id);

            /* do hash calculate */
            var hashResult = this._Hash.GetHashResult(passwordFromDB);

            /* check if consistent */
            return hashResult == password;
        }
    }

    public class FootballStrikeValidator : LiveGameValidator
    {
        public FootballStrikeValidator(IAccount account, IHash hash) : base(account, hash)
        {
            AcceptId = new List<int>(){ 111, 222 };
        }
    }

    [TestFixture]
    public class LiveGameValidatorTest
    {
        [Test]
        public void checkAuthenticationTest()
        {
            IAccount account = new StubAccount();
            IHash hash = new StubHash();

            LiveGameValidator liveGameValidator = new FootballStrikeValidator(account, hash);
            string id = "111";
            string pwd = "222";
            Assert.AreEqual(liveGameValidator.checkAuthentication(id, pwd), true);
        }

        [Test]
        public void checkIdTest()
        {
            IAccount account = new StubAccount();
            IHash hash = new StubHash();

            LiveGameValidator liveGameValidator = new FootballStrikeValidator(account, hash);
            string id = "333";
            string pwd = "222";
            Assert.AreEqual(liveGameValidator.checkAuthentication(id, pwd), false);
        }
    }
}
