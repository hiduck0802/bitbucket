﻿using System;

namespace IOC_DI
{
    public interface IHash
    {
        string GetHashResult(string passwordFromDB);
    }

    public class Hash : IHash
    {
        public string GetHashResult(string password)
        {
            throw new NotImplementedException();
        }
    }

    public class StubHash : IHash
    {
        public string GetHashResult(string password)
        {
            return "222";
        }
    }

    
}
