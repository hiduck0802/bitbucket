﻿using System;

namespace IOC_DI
{
    public interface IAccount
    {
        string getPassword(string id);
    }
    public class Account : IAccount
    {
        public string getPassword(string id)
        {
            throw new NotImplementedException();
        }
    }

    public class StubAccount : IAccount
    {
        public string getPassword(string id)
        {
            return "123";
        }
    }
}
