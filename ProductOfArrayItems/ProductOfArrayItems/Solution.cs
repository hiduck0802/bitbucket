﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductOfArrayItems
{
    public class Solution
    {
        public int Product(int[] values)
        {
            if (values == null)
            {
                throw new ArgumentNullException();
            }
            else if (values.Length == 0)
            {
                throw new InvalidOperationException();
            }
            else
            {
                int result = 1;
                foreach (var num in values)
                {
                    result *= num;
                }

                return result;
            }
        }
    }
}
