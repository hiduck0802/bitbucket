﻿using System;

namespace ProductOfArrayItems
{
    public class ArgumentNullException : Exception
    {
        public ArgumentNullException():base("Exception: null input!"){}
    }
}
