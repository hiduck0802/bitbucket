﻿using System;

namespace ProductOfArrayItems
{
    public class InvalidOperationException : Exception
    {
        public InvalidOperationException():base("Exception: Input length is zero!") { }
    }
}
