﻿using NUnit.Framework;
using ProductOfArrayItems;

namespace ProductOfArrayItemsTest
{
    
    public class ProductOfArrayItemsTest
    {
        [TestCase(new int[] { 5, 4, 1, 3, 9 })]
        [TestCase(new int[] { -5, 4, -1, 3, 9 })]
        public void TestBasicProductFeature(int[] values)
        {
            Solution solution = new Solution();
            Assert.AreEqual(540, solution.Product(values));
        }

        [Test]
        public void TestLengthZero()
        {
            Solution solution = new Solution();
            Assert.Throws<InvalidOperationException>(() => solution.Product(new int[] { }));
        }

        [Test]
        public void TestNullInput()
        {
            Solution solution = new Solution();
            Assert.Throws<ArgumentNullException>(() => solution.Product(null));
        }

    }
}
