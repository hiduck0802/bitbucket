﻿using FindTheParityOutlier;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace FindTheParityOutlierTests
{
    public class FindTheParityOutlierTests
    {
        [TestCase(new int[] { 2, 6, 8, -10, 3 })]
        [TestCase(new int[] { 3 })]
        [TestCase(new int[] { 0, 48, 3 })]
        public void TestMethod1(int[] integers)
        {
            Solution solution = new Solution();
            Assert.IsTrue(3 == solution.Find(integers));
        }

        [TestCase(new int[] { 206847684, 1056521, 7, 17, 1901, 21104421, 7, 1, 35521, 1, 7781 })]
        public void TestBigInteger(int[] integers)
        {
            Solution solution = new Solution();
            Assert.IsTrue(206847684 == solution.Find(integers));
        }

        [TestCase(new int[] { int.MaxValue, 0, 1 })]
        public void TestMaxBoundary(int[] integers)
        {
            Solution solution = new Solution();
            Assert.IsTrue(0 == solution.Find(integers));
        }

        [TestCase(new int[] { int.MinValue, 0, 1 })]
        public void TestMinBoundary(int[] integers)
        {
            Solution solution = new Solution();
            Assert.IsTrue(1 == solution.Find(integers));
        }

        [TestCase(new int[] { })]
        public void TestNullInput(int[] integers)
        {
            Solution solution = new Solution();
            Assert.Throws<NullInputException>(() => solution.Find(integers));
        }

        [TestCase(new int[] { 1, 3, 5, 8, 10 })]
        [TestCase(new int[] { 2, 22, 200, 58, 99, 103, 3 })]
        public void TestNotFound(int[] integers)
        {
            Solution solution = new Solution();
            Assert.Throws<NotFoundException>(() => solution.Find(integers));
        }
    }
}