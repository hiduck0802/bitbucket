﻿namespace FindTheParityOutlier
{
    public class Solution
    {
        public int Find(int[] integers)
        {
            if (integers.Length == 0)
                throw new NullInputException();
            

            int oddIndex = -1;
            int evenIndex = -1;
            int oddCount = 0;
            int evenCount = 0;

            for (var i = 0; i < integers.Length; i++)
            {
                if (integers[i] % 2 == 0)
                {
                    evenIndex = i;
                    evenCount++;
                }
                else
                {
                    oddIndex = i;
                    oddCount++;
                }
            }

            if (evenCount == 1)
                return integers[evenIndex];

            if(oddCount == 1)
                return integers[oddIndex];

            throw new NotFoundException();
        }
    }
}