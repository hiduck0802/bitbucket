﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindTheParityOutlier
{
    public class NullInputException : Exception
    {
        public NullInputException() : base("Exception: null input array!") { }
    }
}
