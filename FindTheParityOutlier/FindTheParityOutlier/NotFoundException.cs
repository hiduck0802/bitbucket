﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindTheParityOutlier
{
    public class NotFoundException : Exception
    {
        public NotFoundException() : base("The only one odd number or even number is not found!") { }
    }
}
