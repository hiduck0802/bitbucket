﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistentBugger
{
    public class Solution
    {
        private int countUntilLengthIsOne = 0;
        private long multiplyResult = 1;
        
        static void Main(string[] args)
        {
        }

        public int Persistence(long n)
        {
            /* 
             * @param n: positive parameter num
             * @return: number of times that multiply the digits in num until get a single digit 
             */

            long updateN = n;
            countUntilLengthIsOne = 0;
            if (updateN.ToString().Length == 1)
            {
                return 0;
            }
            else
            {
                if (updateN.ToString().Contains('0'))
                {
                    return 1;
                }
                else
                {
                    while (updateN.ToString().Length > 1)
                    {
                        multiplyResult = 1;
                        foreach (var ch in updateN.ToString())
                        {
                            multiplyResult *= (int)Char.GetNumericValue(ch);
                        }

                        countUntilLengthIsOne += 1;
                        if (multiplyResult.ToString().Contains('0'))
                        {
                            return countUntilLengthIsOne + 1;
                        }
                        else
                        {
                            updateN = (long)multiplyResult;
                        }
                    }

                    return countUntilLengthIsOne;
                }
            }
        }
    }
}
