﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PersistentBugger;

namespace PersistentBuggerTest
{
    [TestClass]
    public class PersistentBuggerTest
    {
        Solution solution = new Solution();

        [TestMethod]
        public void TestGenernal()
        {
            Assert.AreEqual(3, solution.Persistence(39));
            //Assert.AreEqual(4, solution.Persistence(999));
        }

        [TestMethod]
        public void TestAlreadyOneDigit()
        {
            Assert.AreEqual(0, solution.Persistence(4));
            Assert.AreEqual(0, solution.Persistence(0));
            Assert.AreEqual(0, solution.Persistence(1));
        }

        [TestMethod]
        public void TestZeroInProcess()
        {
            Assert.AreEqual(2, solution.Persistence(825));
            Assert.AreEqual(2, solution.Persistence(25));
        }

        [TestMethod]
        public void TestIntegerBoundary()
        {
            Assert.AreEqual(2, solution.Persistence(2147483647));
            // 2147483647 -> 56*32*18*28 = 903168 
            // 903168 -> 0
        }

        [TestMethod]
        public void TestLongBoundary()
        {
            Assert.AreEqual(1, solution.Persistence(9223372036854775807));
            Assert.AreEqual(2, solution.Persistence(9223371936854775797));
            // 9223371936854775797 -> 36*63*27*960*49*35*63 = 6351593875200
            // 6351593875200 -> 0
        }
    }
}
