﻿using System;

namespace CatAndMouse
{
    public class CatAndMouse
    {
        public string CatMouse(string x)
        {
            int cat_index = -1;
            int mouse_index = -1;

            for (int i = 0; i < x.Length; i++)
            {
                if (x[i] == 'C')
                    cat_index = i;

                if (x[i] == 'm')
                    mouse_index = i;

                if (cat_index != -1 && mouse_index != -1)
                    break;
            }

            string result = (Math.Abs(mouse_index - cat_index) > 3) ? "Escape!" : "Caught!";
            return result;
        }
    }
}