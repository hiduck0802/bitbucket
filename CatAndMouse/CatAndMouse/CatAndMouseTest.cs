﻿using NUnit.Framework;

namespace CatAndMouse
{
    public class CatAndMouseTest
    {
        [TestCase("C....m")]
        [TestCase("m....C")]
        [TestCase("m...C")]
        [TestCase("C...........m")]
        public void EscapeTest(string x)
        {
            CatAndMouse catAndMouse = new CatAndMouse();
            Assert.AreEqual("Escape!", catAndMouse.CatMouse(x));
        }

        [TestCase("C.m")]
        [TestCase("m..C")]
        [TestCase("Cm")]
        public void Caughtest(string x)
        {
            CatAndMouse catAndMouse = new CatAndMouse();
            Assert.AreEqual("Caught!", catAndMouse.CatMouse(x));
        }
    }
}