﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Find_the_odd_int;

namespace Find_the_odd_int_Test
{
    [TestClass]
    public class UnitTest
    {
        Solution solution = new Solution();

        [TestMethod]
        public void TestGenernal()
        {
            int[] seq = new int[17]{20, 1, -1, 2, -2, 3, 3, 5, 5, 1, 2, 4, 20, 4, -1, -2, 5};
            Assert.AreEqual(5, solution.find_it(seq));
        }

        [TestMethod]
        public void TestOnlyContainOneNum()
        {
            int[] seq = new int[1]{3};
            Assert.AreEqual(3, solution.find_it(seq));
        }

        [TestMethod] 
        public void TestAppearContiousTimes()
        {
            int[] seq = new int[15]{-1, -1, -1, 2, 2, 100, 23, -4, 55, -10, -4, 100, 55, -10, 23};
            Assert.AreEqual(-1, solution.find_it(seq));
        }

        [TestMethod]
        [ExpectedException(typeof(NotOnlyOneAppearOddNumException),
            "Exception: Not only one number appear odd times!")]
        public void TestOverOneAppearOddTimesNum()
        {
            int[] seq = new int[4]{1, 60, 60, 30};
            solution.find_it(seq);
        }

        [TestMethod]
        [ExpectedException(typeof(NotOnlyOneAppearOddNumException),
            "Exception: Not only one number appear odd times!")]
        public void TestNullInputSeq()
        {
            int[] seq = new int[0] {};
            solution.find_it(seq);
        }
    }
}
