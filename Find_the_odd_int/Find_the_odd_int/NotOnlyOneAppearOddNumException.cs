﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Find_the_odd_int
{
    public class NotOnlyOneAppearOddNumException : Exception
    {
        public NotOnlyOneAppearOddNumException():base("Exception: Not only one number appear odd times!"){}
    }
}
