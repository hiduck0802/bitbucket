﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Find_the_odd_int
{
    public class Solution
    {
        static void Main(string[] args)
        {
        }

        public int find_it(int[] seq)
        {
            // check null array
            if (seq.Length == 0)
            {
                throw new NotOnlyOneAppearOddNumException();
            }

            Dictionary<int, int> num_of_count = new Dictionary<int, int>();
            int numAppearOddTimes = -1;

            foreach (var n in seq)
            {
                if (!num_of_count.ContainsKey(n))
                {
                    num_of_count.Add(n, 1);
                }
                else
                {
                    num_of_count[n] += 1;
                    if (num_of_count[n] == 2)
                    {
                        num_of_count[n] = 0;
                    }
                }
            }

            foreach (var key in num_of_count.Keys)
            {
                if (num_of_count[key] == 1)
                {
                    if (numAppearOddTimes != -1)
                    {
                        throw new NotOnlyOneAppearOddNumException();
                    }

                    numAppearOddTimes = key;
                }
            }

            return numAppearOddTimes;
        }
    }
}
