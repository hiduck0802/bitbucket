﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumOfDigits_DigitalRoot
{
    public class Solution
    {
        private long _tmp;
        private int _res;

        public int DigitalRoot(long n)
        {
            _tmp = n;
            while (true)
            {
                _res = 0;
                foreach (var ch in _tmp.ToString())
                {
                    _res += (int)Char.GetNumericValue(ch);
                }

                _tmp = _res;
                if (_tmp.ToString().Length == 1)
                {
                    break;
                }
            }

            return _res;
        }
    }
}
