﻿using NUnit.Framework;
using SumOfDigits_DigitalRoot;

namespace SumOfDigits_DigitalRoot_Tests
{
    public class SumOfDigits_DigitalRoot_Tests
    {
        [TestCase(456, Description="456 should return 6")]
        [TestCase(942, Description = "942 should return 6")]
        [TestCase(132189, Description = "132189 should return 6")]
        public void TestResultIs6(long n)
        {
            Solution solution = new Solution();
            Assert.AreEqual(6, solution.DigitalRoot(n));
        }
        
        [TestCase(0, Description = "0 should return 0")]
        public void TestResultIs0(long n)
        {
            Solution solution = new Solution();
            Assert.AreEqual(0, solution.DigitalRoot(n));
        }
    }
}
